from pydantic import BaseSettings
from pathlib import Path

ENV_PATH = Path.cwd() / '.env'


class Settings(BaseSettings):
    host: str
    user_service: str
    google_client_id: str
    google_client_secret: str
    session_middleware_key: str

    class Config:
        env_file = ENV_PATH


settings = Settings()
