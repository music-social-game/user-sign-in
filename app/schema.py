from pydantic import BaseModel, EmailStr
from typing import Optional


class User(BaseModel):
    username: str
    password: str
    email: EmailStr


class LoginUser(User):
    email: Optional[EmailStr]
    username: Optional[str]