from fastapi import FastAPI, status, Request
import uvicorn
import aiohttp
from fastapi.exceptions import HTTPException
from .config import settings
from pydantic import UUID4
from app.schema import User, LoginUser
from authlib.integrations.starlette_client import OAuth
from starlette.config import Config
from starlette.middleware.sessions import SessionMiddleware


app = FastAPI()
oauth_conf = Config('.env')
oauth = OAuth(oauth_conf)
oauth.register(
    name='google',
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration',
    client_kwargs={
        'scope': 'openid email profile'
    }
)
app.add_middleware(SessionMiddleware, secret_key=settings.session_middleware_key)
session = aiohttp.ClientSession()


@app.on_event("shutdown")
async def shutdown():
    await session.close()


@app.get("/")
async def root():
    return {"message": "hello world"}


@app.post("/signup", status_code=status.HTTP_202_ACCEPTED)
async def signup(user_data: User):
    user_dict = user_data.dict()
    user_dict.update({'confirmed': False})
    async with session.post(f'{settings.user_service}make_user', json=user_dict) as resp:
        if resp.status == 400:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, (await resp.json())['detail'])
        elif resp.status == 200:
            return {"message": "Confirmation e-mail sent"}


@app.get("/confirm/{uuid}", status_code=status.HTTP_202_ACCEPTED)
async def confirm_email(uuid: UUID4):
    async with session.put(f'{settings.user_service}confirm_email', params={"uuid": str(uuid)}) as resp:
        if resp.status == 200:
            return {"message": "E-mail confirmed, user is active"}
        else:
            raise HTTPException(status.HTTP_400_BAD_REQUEST)


@app.post("/login", status_code=status.HTTP_200_OK)
async def login(user_data: LoginUser):
    async with session.post(f'{settings.user_service}login_email', json=user_data.dict()) as resp:
        if resp.status in (401, 400):
            raise HTTPException(status.HTTP_400_BAD_REQUEST, (await resp.json())['detail'])
        elif resp.status == 200:
            json_data = await resp.json()
            return json_data
    raise HTTPException(status.HTTP_400_BAD_REQUEST)


@app.post("/refresh_token", status_code=status.HTTP_200_OK)
async def refresh_token(request: Request):
    async with session.post(f'{settings.user_service}refresh_token', headers=request.headers) as resp:
        data = await resp.json()
        return data


@app.route('/google_login')
async def google_login(request: Request):
    redirect_uri = request.url_for('social_auth')
    await oauth.google.authorize_redirect(request, redirect_uri)
    return await oauth.google.authorize_redirect(request, redirect_uri)


@app.get('/social_auth')
async def social_auth(request: Request):
    token = await oauth.google.authorize_access_token(request)
    user = await oauth.google.parse_id_token(request, token)
    user_dict = {'confirmed': True, 'username': user['name'], 'email': user['email']}
    async with session.post(f'{settings.user_service}social_auth', json=user_dict) as resp:
        if resp.status == 200:
            return await resp.json()
        raise HTTPException(status.HTTP_400_BAD_REQUEST, (await resp.json())['detail'])

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
